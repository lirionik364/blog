from django import forms

from .models import Post


def title():
    return forms.CharField(
        label="",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "title",
            },
        ),
    )


def content():
    return forms.CharField(
        label="",
        max_length=100,
        widget=forms.Textarea(
            attrs={"class": "form-control", "placeholder": "content"},
        ),
    )


class PostForm(forms.ModelForm):
    title = title()
    content = content()

    class Meta:
        model = Post
        fields = ("title", "content", "status")
