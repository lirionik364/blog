from ..posts.models import Post, Category

from rest_framework import viewsets, serializers, mixins


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.SerializerMethodField()

    def get_name(self, name):
        return f'Name of Category: {name}'

    class Meta:
        model = Category
        fields = '__all__'


class PostSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.SerializerMethodField()
    title = serializers.CharField()

    def get_name(self, name):
        return f'Name of Post: {name}'

    class Meta:
        model = Post
        fields = '__all__'

# class DishSerializer(serializers.HyperlinkedModelSerializer):
#     name = serializers.SerializerMethodField()

#     def get_name(self, name):
#         return f'Name of Dish: {name}'

#     class Meta:
#         model = Dish
#         fields = '__all__'
