from django.urls import path

from . import views


app_name = "posts"

urlpatterns = [
    path("", views.PostList.as_view(), name="index"),
    path("add/", views.PostCreate.as_view(), name="add"),
    path("<int:pk>", views.PostDetail.as_view(), name="details"),
    path("update/<int:pk>", views.PostUpdate.as_view(), name="update"),
    path("delete/<int:post_id>", views.delete_post, name="delete"),
]
