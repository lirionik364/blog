from django.shortcuts import redirect, get_object_or_404
from django.views.generic import ListView, DetailView, CreateView, UpdateView

from .models import Post
from .forms import PostForm


class PostList(ListView):
    model = Post
    template_name = "posts/index.html"
    context_object_name = "posts"

    def get_queryset(self):
        return Post.objects.prefetch_related("pc__category")


class PostDetail(DetailView):
    model = Post
    template_name = "posts/details.html"
    context_object_name = "post"


class PostCreate(CreateView):
    model = Post
    template_name = "posts/add.html"
    form_class = PostForm


class PostUpdate(UpdateView):
    model = Post
    template_name = "posts/update_post.html"
    form_class = PostForm


def delete_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    post.delete()
    return redirect("posts:index")
